XMLF90
======

xmlf90 is a suite of libraries to handle XML in Fortran. It has two
major components:

- A XML parsing library. The parser was designed to be a useful
  tool in the extraction and analysis of data in the context of
  scientific computing, and thus the priorities were efficiency and the
  ability to deal with very large XML files while maintaining a small
  memory footprint. The most complete programming interface is
  based on the very successful SAX (Simple API for XML) model,
  although a partial DOM interface and a very experimental XPATH interface
  are also present.
- A library (xmlf90_wxml) that facilitates the writing of well-formed
  XML, including such features as automatic start-tag completion,
  attribute pretty-printing, and element indentation. There are also
  helper routines to handle the output of numerical arrays.
  Layered on top of this library is xmlf90_cml, a library to generate
  Chemical-Markup-Language (CML) files.

Documentation is available in doc/Tutorial, and can be seen online
in [https://xmlf90.readthedocs.io](https://xmlf90.readthedocs.io).


INSTALLATION
------------

A) With CMake:

    cmake -S. -B_build -DCMAKE_INSTALL_PREFIX=/path/to/install/directory
    cmake --build _build
    (push _build; ctest ; popd)  # To run a simple test
    cmake --install _build

B) With auto-tools

    ./configure --prefix=/path/to/install/directory
    make
    make check
    make install

You can go into the subdirectories 'doc/Examples' and explore, and go
into 'doc/Tutorial' and try the exercises in the User Guide.


COMPILING USER PROGRAMS
-----------------------

A) With CMake

Just use the standard CMake idiom in your CMakeLists.txt file:

    add_executable(your_program your_sources)
    find_package(xmlf90 REQUIRED)
    target_link_libraries(your_program xmlf90::xmlf90)

The above assumes that the installation directory for xmlf90 can
be found by CMake. This can be achieved by adding it to the CMAKE_PREFIX_PATH
CMake or enviroment variable:

    cmake -S. -B_your_build -DCMAKE_PREFIX_PATH=$XMLF90_ROOT .......
    CMAKE_PREFIX_PATH=$XMLF90_ROOT cmake -S. -B_your_build .......

B) With a makefile

Both methods above will create a pkg-config file with information
for client programs. It is suggested that the user create a
separate directory to hold the program files and prepare a Makefile
following this example (FC, FFLAGS, and LDFLAGS need to be set appropriately
for the Fortran compiler used):

    #---------------------------------------------------------------
    #
    default: example
    #
    #---------------------------
    XMLF90_ROOT=/path/to/installation
    PKG_CONFIG_PATH=$(XMLF90_ROOT)/lib/pkgconfig:$(PKG_CONFIG_PATH)
    #
    XMLF90_LIBS=$(pkg-config --libs xmlf90)
    XMLF90_INCFLAGS=$(pkg-config --cflags xmlf90)
    #
    INCFLAGS := $(XMLF90_INCFLAGS) $(INCFLAGS)
    LIBS:= $(XMLF90_LIBS) $(LIBS)
    #---------------------------
    #
    OBJS= m_handlers.o example.o
     
    example:  $(OBJS)
            $(FC) $(LDFLAGS) -o $@ $(OBJS)  $(LIBS)
    #
    clean: 
            rm -f *.o example *.mod
    #
    # Building rules
    #
    .F.o:
            $(FC) -c $(FFLAGS) $(INCFLAGS)  $(FPPFLAGS) $<
    .f.o:
            $(FC) -c $(FFLAGS) $(INCFLAGS)   $<
    .F90.o:
            $(FC) -c $(FFLAGS) $(INCFLAGS)  $(FPPFLAGS) $<
    .f90.o:
            $(FC) -c $(FFLAGS) $(INCFLAGS)   $<
    #
    #---------------------------------------------------------------

Here it is assumed that the user has two source files,
'example.f90' and 'm_handlers.f90'. Simply typing
'make' will compile 'example', pulling in all the needed
modules and library objects.


NOTE: The FoX project, started by Toby White and now maintained by
Andrew Walker, has produced a more robust and feature-rich package
starting from xmlf90, although not as fast and trim in some areas.  It
can be found in [https://github.com/andreww/fox](https://github.com/andreww/fox).

